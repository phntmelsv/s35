// Steps
// 1. npm init in the directory via the terminal
// 2. sudo npm install express mongoose
// 3. sudo npm install dotenv
// 4. touch index.js - terminal
// 5. touch .gitignore - terminal
// 6. touch .env - terminal
// 7. code in index.js (see below)
// 8. nodemon index.js - terminal
// 9. test in Postman!


const express = require("express")
const mongoose = require("mongoose")
require('dotenv').config() // Initialize ENV file
const app = express()
const port = 3001

// Mongoose connection
mongoose.connect(`mongodb+srv://nillyp:${process.env.MONGODB_PASSWORD}@zuittm0.qy65ozq.mongodb.net/b271-users-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection 

// Listeners
db.on("error", () => console.log("Connection error"))
db.once("open", () => console.log("Connected to MongoDB!"))

// [SECTION] Schemas
const user_schema = new mongoose.Schema({
	username: String,
	password: String
})

// [SECTION] Models
const User = mongoose.model("User", user_schema)

// Middleware registration
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// [SECTION] Routes
// Create new user
app.post('/signup', (request, response) => {
	User.findOne({name: request.body.username, password: request.body.password}).then((result, error) => {
		// Duplicates handling

		if(result != null && result.username == request.body.username){
			return response.send("Duplicate user found!")
		} else { 

			// 1. Create a new instance
			let new_user = new User({
				username: request.body.username, 
				password: request.body.password 
			})

			// 2. Save to database and handle errors
			new_user.save().then((created_user, error) => {
				if(error){
					return console.log(error)
				} else {
					return response.status(201).send("New user registered")
				}
			})
		}
	})
})

// Get all users
app.get("/signup", (request, response) => {
	User.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		} 

		return response.status(200).send(result)
	})
})

app.listen(port, () => console.log(`The server is running at localhost:${port}`))


